#+TITLE: Configuring IPv4 Addressing and Static Routes
* Intro
The first step is to configure an IP address:
> ip address IP MASK

At this point, the router will add two routes: a connected route and a local route.  This is assuming that the interface is up.
* Types Of Routes
Connected route - A route that points to the subnet of the interface's IP addresses.
Local route - A route with mask /32 that points directly to the interface for efficienty reasons

To configure a static route:
> ip route IP MASK {IFACE|NEXT_HOP_IP} [ADMIN_DISTANCE]

When configuring an Ethernet interface, the next hop IP option should be used.

The four types of static routes are netowrk, host, floating, and default.

Network Route:
A route that mataches an entire subent (or class A, B, or C network).
Use the subnet ID and either the outgoing interface or next hop IP.
If using the outgoing interface option, the 'show ip route static' command will list this route as directly connected.

Host Route:
A route to a single host with a /32 mask.
Typically used with multiple paths to a subnet where you can configure a route to a single host over one path and the rest of the subnet over the other path.

Floating Route:
These are multiple routes to the same place with differing administrative distances used as backups.  Lower AD is better.
Default administrative distance is 1 for static routes.

Default Route:
A route that matches all packets when no other route will.  When configuring, use all zeroes for IP and mask.

You can force a static route into the routing table by slapping the keyoword 'permanent' at the end of an 'ip route' command.  This will bypass IOS's checks that it does before adding routes to the table.

* Troubleshooting
Symptom: Incorrect static routes that appear in the IP routing table.
Check the following things:
- Is there a subnetting math error in the subnet ID and mask?
- Is the next-hop IP address correct and referencing an IP address on a neighboring router?
- Does the next-hop IP address identify the correct router?
- Is the outgoing interface correct, and referencing an interface on the local router (that is, the same router where the static route is configured)?

Symptom: The static route does not appear in the routing table.
IOS consideres the following two things before adding a static route to the routing table:
- For 'ip route' commands that list an outgoing interface, that interface must be in an up/up state.
- For 'ip route' commands that list a next-hop IP address, the local router must have a route to reach that next-hop address.

Symptom: The correct static route appears but works poorly.
This can happen when using the 'permanent' keyword.  Check that the outgoing interface is up and that there is a route to the destination.
--------------------------------

* Misc.
If multiple route to the same place exist, IOS will choose the one with the longest prefix match.  Causes for multiple routes include static routes, route autosummarization, and manual route summarization.

Use this command to see which route IOS will choose.  Omit parameter to show routing table.
> show ip route [connected|static|ospf|IP_ADDR]
