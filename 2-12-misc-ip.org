* First Hop Redundancy Protocols (FHRP)
A group of protocols used by routers for high availability.
Eatch FHRP makes the following happen:
1. All hosts act like they always have, with one default router wetting that never has to change.
2. The default routers share a virtual IP address in the subnet, defined by the FHRP.
3. Hosts use the FHRP virtual IP address as their default router address.
4. The routers exchange FHRP protocol messages so that both agree as to which router does what work at any point in time.
5. When a router fails or has some other problem, the routers use the FHRP to choose which router takes over responsibilities from the failed router.

The protocols:
| Acronym | Full Name                    | Origin   | Redundancy Approach | Load Balancing Per |
|---------+------------------------------+----------+---------------------+--------------------|
| HSRP    | Hot Standby Router Protocol  | Cisco    | active/standby      | Subnet             |
| VRRP    | Virtual Router Redun. Proto. | RFC 5798 | active/standby      | Subnet             |
| GLBP    | Gateway Lad Bal. Proto.      | Cisco    | active/active       | Host               |

* Simple Network Management Protocol
SNMP messages are exchanged between "managers" and "agents".
Management applications are run on Network Management Stations (NMS).

Trap - The original message.  Sent via UDP, no reliability.
Inform - Introduced in v2.  Still uses UDP, but adds application-layer reliability.

The Management Information Base (MIB) is a database of variables (object IDs (OID)) containing information about a particular host.

SNMPv1 and SNMPv2c has the concept of Communities for security.
The two communities are Read-Only and Read/Write.

* (T)FTP
Allows copying files to and from the local filesystem.
To view local filesystems: 'show file systems'.

There are shortucts you already know to viewing specific files:
- show running-config - system:running-config
- show startup-config - nvram:startup-config
- show flash - Usually flash0:

Upgrading the IOS images with FTP is the same as TFTP, except it looks more like the scp command with the server IP and filename included in the command, rather than with prompts.o

Some actions that can be performed with FTP:
- Navigate directories
- Add/remove directories
- List files
- File transfer

To upgrade IOS, download the image to the TFTP server, then from the router/switch: 'copy tftp flash'.
It will then prompt you for the server and file name.
Remember FTP uses to connections, Control and Data.
