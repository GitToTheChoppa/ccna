#+TITLE: Analyzing Ethernet LAN Switching
* Switching Logic
1. Deciding when to forward a frame or when to filter a frame, based on the distination MAC address.
2. Preparing to forwared frames by learning MAC addresses by examining the source MAC address of each frame received by the switch.
3. Preparing to forward only one copy of the frame to the destination by creating an L2 loop-free environment with other switches via STP.
* Show Commands
Show statistics on the frames coming in and leaving the port:
> show interface INT counters

Search MAC table by address:
> show mac address-table dynamic address MAC_ADDR

See what's on the port:
> show mac address-table dyamic interface IF

* MAC Address Table Management
Set table entry aging (time until entry is automatically removed, default 300s).
> mac address-table aging-time SECONDS

* Clear MAC Table
> clear mac address-table dynamic [vlan VLAN] [interface IF] [address ADDR]
