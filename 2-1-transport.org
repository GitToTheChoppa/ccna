#+TITLE: Introduction to TCP/IP Transport and Applications
* Sockets
A socket consists of 3 things:
1. An IP address
2. A transport protocol (TCP or UDP)
3. A port number

* Port Number Ranges
0 - 1023: Well Known (System) Ports
Assigned by IANA with a stricter review process to assign new ports than user ports.

1024 - 49151: User (Registered) Ports
Assigned by IANA with a less strict process to assign new ports compared to well-known ports.

49152 - 65535: Ephemeral (Dynamic, Prive) Ports
Not assigned and intended to be dynamically allocated and used temporarily for a client application while the app is running.

* Ports
| Port# | Protocol | Application |
|-------+----------+-------------|
|    20 | TCP      | FTP Data    |
|    21 | TCP      | FTP Control |
|    22 | TCP      | SSH         |
|    23 | TCP      | Telnet      |
|    25 | TCP      | SMTP        |
|    53 | Both     | DNS         |
|    67 | UDP      | DHCP Server |
|    68 | UDP      | DHCP Client |
|    69 | UDP      | TFTP        |
|    80 | TCP      | HTTP        |
|   110 | TCP      | POP3        |
|   161 | UDP      | SNMP        |
|   443 | TCP      | SSL         |
|   514 | UDP      | Syslog      |
Note DNS uses port 53 for both TCP and UDP.
* Connection Establishment and Termination
Connections exists between two sockets.
The IP address related fields of a socket are inferred from L3.
The port related fields of a socket are inferred from L4.
The port number field is the variable.

The three-way handshake is a process that initiates a (socket-to-socket) connection.
------------------------------
Browser		Server
------------------------------
----->>> SYN >>>-----
-----<<< SYN, ACK <<<-----
----->>> ACK >>>-----
------------------------------
SYN = SYNchronize the sequence numbers
ACK = ACKnowledge
SYN and ACK are flags in the TCP header.
* Error Receovery/Reliability
TCP accomplishes reliability with the Sequence and Acknowloegement fields of the TCP header.
Reliability is acheived in both directions by using the Sequence Number fields of one direction combined with the Acknowledgmend fields in the opposite direction.

The server keeps track of the number of bytes received from the host.
When the server sends an ACK, TCP adds a number to the header.  This number is  the byte number it expects to received next;  known as forward acknowledgment.
* Error Recovery
TCP uses the Seq and Ack fields so the receiving host can notice lost data, ask the sending host to resend, and then acknowlede that the re-sent data arrived.
The receipt of an ack that does not ack all the data sent so far tells the sending host to resend the data.
The retransmission timer decides how long to wait for an ack if it's not receiving on that's expected.  It will then re-send the unacked data.
* Flow Control
The concept of Windowing defines an amount of data that can be outstanding and awaiting acknowledgment at any oint point in time.

The receiver can slide the window to allow more or lesss unacked data; known as a Sliding Window or Dynamic Window.


Applications that use UDP are either tolerant of data loss, or have some application mechanism to revcover lost data.

The receiving host looks at multiple fields - one per header - to identify the next header or field in the received message.
The Ethernet Type field ids the type of header that follows the Ethernet header - IPv4 or IPv6.

The IP header contains a similar field called the IP Protocol field that ids either:
6 = TCP
17 = UDP
