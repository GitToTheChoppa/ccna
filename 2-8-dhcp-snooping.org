#+TITLE: DHCP Snooping and ARP Inspection
* DHCP Snooping
** Concepts
DHCP snooping operates with information from both the DHCP header and the L2PDU.

- Trusted Port - Connected to the DHCP server as well as devices like routers and internal servers.
- Untrusted Port - Connected to hosts usually requiring DHCP.

When it comes to the various DHCP messages (DORA):
- Clients send DISCOVER & REQUEST
- Servers send OFFER & ACK
** Sample Attack: Spurious DHCP Servers
In this this scenario, the attacker falsely claims to be the DHCP server, makes itself the default router, thus becomes the man-in-the-middle for all packets outside the subnet.
*** How It Works
1. A legit PC boots and looks for a DHCP server via broadcast (DHSCPDISCOVER).
2. The attacker's PC replies with the usual DHCPOFFER.
** Logic
When determining if a packet needs to be filtered:
1. Examine all incoming DHCP messages.
2. If normally sent by servers, discard the messages.
3. If normally sent by clients:
3. a. For DISCOVER and REQUEST messages, check for MAC address consistency between the L2 frame and the DHCP message.
3. b. For RELEASE or DECLINE messages, check the incoming interface plus IP address versus the DHCP Snooping binding table.
4. For messages not filtered that result in a DHCP lease, build a new entry to the DHCP Snooping binding table.
** Filtering DISCOVER Messages Based On MAC Address
This is used to counter attacks that involve an attacker's device requesting multiple IP addresses via DHCP.
This will lead to a saturated DHCP range as well as wasted switch CPU power.

The information that the switch uses to decide whether or not to filter a particular DHCP message:
1. chaddr - 'Client Hardware ADDRress' - A value found in DHCP that is based on the source MAC.
2. The actual source MAC found in the L2PDU.

If the above values do not match, discard that bitch.
** The DHCP Snooping Binding Table
Each record of the table lists this information about a particular DHCP connection that DHCP Snooping has already allowed through:
- L2PDU source IP
- IP Address to be leased
- VLANID
- Interface (gig1/0/1)
** Filter Messages That Release IP Addresses
In normal DHCP operations, a host can send a RELEASE to give up its leased DHCP IP.

An attacker can attempt to release a legit hosts IP, hoping that the DHCP server re-assign that IP to the attacker.
Another man-in-the-middle attack.
Therefore, if RELEASE comes from a different interface than what's listed in the binding table, there's probably an attack going on.
Remember, binding table entries are made post DHCP Snooping.
** Config Breakdown
1. Both of the following commands are required to enable snooping:
   gc> ip dhcp snooping
   gc> ip dhcp snooping vlan VLAN_LIST
2. On switches acting as a DHCP relay, disable the insertion of DHCP Option 82 data into DHCP messages:
   gc> no ip dhcp snooping information option
3. On ports connected to trusted devices, especially the DHCP server:
   if> ip dhcp snooping trust
4. Optional: configure a rate limit and auto-recover from err-disabled after a violation:
   if> ip dhcp snooping limit rate MAX_PKTS_PER_SEC
   gc> errdisable recovery cause dhcp-rate-limit
   Set the number of seconds before recovering from err-disabled automatically:
   gc> errdisable recovery interval SEC
* Dynamic ARP Inspection (DAI)
DAI examines ARP messages on untrusted ports looking for ports to filter.
It operates on data from:
1. DHCP Snooping binding table
2. Any ARP ACLs
** Normal ARP Operation
When a PC needs to send a packet to an IP with an unknown MAC address, the PC sends a ARP request and waits for the target to reply with its MAC address.
The L3 part of the PDU contains the destiation IP, and the L2 part is an Ethernet broadcast.
Note there are legitimate reasons a host may want to re-broadcast its MAC address without being asked.  This is known as gratuitous ARP.

A gratuitous ARP is:
- An ARP reply message.
- Sent without having received an ARP request.
- Sent to Ethernet broadcast.
** Sample Attack: Gratuitous ARP
If an attackers knows the IP of a host they want to steal packets from, they will send a gratuitous ARP including the target IP and their own MAC address.
If other hosts update their ARP tables to reflect this, the attacker has become the MITM.
** DAI Logic
DAI also uses the concepts of trusted and untrusted ports.
It will look at the source IP and MAC and compare those values to the entry in the DHCP Snooping binding table.
If those valuse are found in the table, DAI allowws the packet, otherwise it's blocked.
When DAI looks for an IP in the binding table and finds a different source MAC than what's in the current packet, there may be an attack going on.

Additionally, DAI can make sure that the source and destination MAC addresses found in the ARP packet matches what's in the Ethernet header.

In summary, DAI will discard packets when:
- Ethernet header source MAC != ARP origin hardware (MAC) address.
- ARP reply messagess have an Ethernet header destination MAC address != ARP target hardware (MAC) address.
- Unexpected IP addresses in the two ARP IP address fields.

DAI can be configured with ARP ACLs which are useful for ports configured with static IP addresses.
** DAI Config
Before configuring DAI:
- Choose whether to rely on DHCP Snooping, ARP ACLs, or both.
- If using snooping, configure it and make the correct ports trusted for DHCP.
- Choose the VLAN(s) on which to enable DAI.
- Make DAI trusted on select ports on those VLANs.  Typically the same as DHCP trusted ports.

DAI can also be configured with rate limits, but there are some small differences, including:
- DAI defaults to use rate limits for all interfaces (trusted and untrusted), with DHCP Snooping defaulting to not use rate limiting.
- DAI allows the configuration of a burst interval (a number of seconds), so that the rate limit can have logic like "x ARP messages over y seconds".  Snooping does not have this feature.
*** Config Breakdown
1. Enable DAI on a switch for specified VLANs:
   gc> ip arp inspection vlan VLAN-LIST
2. Separate from DAI to configure DHCP Snooping and/or ARP ACLs.
3. On ports connected to trusted devices, such as infrastructure:
   if> ip arp inspection trusted
4. (Optional) configure rate limits:
4. a. ip arp inspection limit rate NUM
4. b. ip arp inspection limit rate none
4. c. errdisable recovery cause arp-inspeciton
4. d. errdisable recover interfal SECONDS
5. Optional: Add DAI validation steps:
   gc> ip arp inspection validate {[dst-mac] [src-mac] [ip]}
