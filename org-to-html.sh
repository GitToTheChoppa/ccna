#!/bin/bash

rm -f ./*.html

for CURRFILE in $(/usr/bin/ls)
do
	emacs $CURRFILE --batch -f org-html-export-to-html --kill
done
