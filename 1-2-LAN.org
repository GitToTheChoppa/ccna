#+TITLE: Fundamentals of Ethernet LANs
* Ethernet Lingo
| SPEED     | COMMON NAME         | IEEE INFORMAL | IEEE FORMAL |
|-----------+---------------------+---------------+-------------|
| 10 Mbps   | Ethernet            | 10BASE-T      | 802.3       |
| 100 Mbps  | Fast Ethernet       | 100BASE-T     | 802.2u      |
| 1000 Mbps | Gigabit Ethernet    | 1000BASE-LX   | 802.3z      |
| 1000 Mbps | Gigabit Ethernet    | 1000BASE-T    | 802.3ab     |
| 10 Gbps   | 10 Gigabit Ethernet | 10GBASE-T     | 802.3an     |
* Wire Pairs
** 10BASE-T & 100BASE-T
- PC transmits on pins 1,2.
- PC receives  on pins 3,6.
- Switch transmits on pins 3,6.
- Switch receives on pins 1,2.
** 1000BASE-T
In addition to the pairs used for the earlier standards, we also have pairs at 4,5 & 7,8
* Fiber
** Single Mode
Single-mode fiber bounces one lightbeam down a narrow core. Offers much longer distances, around 40KM.  Slightly more expensive connector hardware.
** Multimode
Multimode fiber bounces multiple light beams at different angles (modes) down a wider (5x) core.  Cable lengths of around 500m.
* The Ethernet Frame
I'm guessing the last row is wrong.
| Byte Len | Field Name            |
|----------+-----------------------|
|        7 | Preamble              |
|        1 | Start Frame Delimeter |
|        6 | Destination MAC       |
|        6 | Source MAC            |
|        2 | Type                  |
|  46-1500 | Data + Pad            |
|        4 | FCS                   |
* MAC Format
MAC addresses are 6-Bytes/48-bit/12-hex-digits
1st 3 bits: Organizatinally Unique Identifier (OUI) given by IEEE.
2nd 3 bits: Serial number given by manufacturer.
* Misc.
Copper wires have length 100m, fiber 5000m.
auto-midx is how switches know if straight-thru/crossover is correct.
